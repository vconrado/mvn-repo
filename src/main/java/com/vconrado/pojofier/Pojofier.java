package com.vconrado.pojofier;

import com.vconrado.pojofier.annotation.IfNullReturnNull;
import com.vconrado.pojofier.annotation.PojoField;
import com.vconrado.pojofier.annotation.PojoIgnore;
import com.vconrado.pojofier.converter.IPojoConverter;
import com.vconrado.pojofier.converter.IPojoRevert;
import com.vconrado.pojofier.converter.PojoFieldConverter;
import com.vconrado.pojofier.converter.PojoFieldRevert;
import com.vconrado.pojofier.exception.PojofierException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Vitor Gomes
 */
public class Pojofier implements Serializable {

    private final static PojofierWrapper INSTANCE = new PojofierWrapper();
    private static Map<String, Object> context = new HashMap();
    private final static int POJIFY = 1;
    private final static int DESPOJIFY = 2;

    public static PojofierWrapper addResource(String key, Object o) {
        Pojofier.context.put(key, o);
        return INSTANCE;
    }

    public static Object getResource(String key) {
        return Pojofier.context.get(key);
    }

    public static <T> List<T> pojify(Class<T> to, List<?> fromList) {
        List<T> list = new ArrayList<>();
        for (Object o : fromList) {
            list.add(Pojofier.pojify(to, o));
        }
        return list;
    }

    public static <T> List<T> despojify(Class<T> to, List<?> fromList) {
        List<T> list = new ArrayList<>();
        for (Object o : fromList) {
            list.add(Pojofier.despojify(to, o));
        }
        return list;
    }

    private void fooMethod() {

    }

    public static <T> T pojify(Class<T> to, Object from) {
        T pojo;
        if (from == null) {
            IfNullReturnNull inrn = to.getAnnotation(IfNullReturnNull.class);
            if (inrn != null) {
                return null;
            }
            try {
                return to.newInstance();
            } catch (InstantiationException | IllegalAccessException ex) {
                System.err.println("Erro ao instanciar objeto a ser retornado (null case): " + ex.getMessage());
                return null;
            }
        }
        try {
            pojo = to.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            System.err.println("Erro ao instanciar objeto a ser retornado: " + ex.getMessage());
            return null;
        }
        for (Field toField : to.getDeclaredFields()) {
            if (toField.getAnnotation(PojoIgnore.class) == null) {
                try {
                    //System.out.println("anotações tipo: " + toField.getAnnotatedType().getType().getTypeName());
                    //System.out.println("Field: " + toField.getName());
                    Method toSetMethod = obtainSetMethod(toField, to);
                    Method fromGetMethod = obtaionGetMethod(toField, toSetMethod, from);
                    // fromGetMethod pode ser null. Neste caso, passe o objeto inteiro
                    invokeMethod(toField, from, fromGetMethod, pojo, toSetMethod, POJIFY);
                } catch (PojofierException ex) {
                    System.out.println("Falha ao pojificar");
                    ex.printStackTrace();
                    return null;
                }
            }
        }
        return to.cast(pojo);
    }

    public static <T> T despojify(Class<T> to, Object from) {
        T pojo;
        if (from == null) {
            IfNullReturnNull inrn = to.getAnnotation(IfNullReturnNull.class);
            if (inrn != null) {
                return null;
            }
            try {
                return to.newInstance();
            } catch (InstantiationException | IllegalAccessException ex) {
                System.err.println("Erro ao instanciar objeto a ser retornado (null case): " + ex.getMessage());
                return null;
            }
        }
        try {
            pojo = to.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            System.err.println("Erro ao instanciar objeto a ser retornado: " + ex.getMessage());
            return null;
        }
        for (Field toField : to.getDeclaredFields()) {
            if (toField.getAnnotation(PojoIgnore.class) == null) {
                try {
                    Method toSetMethod = obtainSetMethod(toField, to);
                    Method fromGetMethod = obtaionGetMethod(toField, toSetMethod, from);
                    invokeMethod(toField, from, fromGetMethod, pojo, toSetMethod, DESPOJIFY);
                } catch (PojofierException ex) {
                    System.out.println("Falha ao despojificar");
                    ex.printStackTrace();
                    return null;
                }
            }
        }
        return to.cast(pojo);
    }

    private static <T> void invokeMethod(Field toField, Object from, Method fromGetMethod, T pojo, Method toSetMethod, int status) throws PojofierException {
        // verifica se o tipo esperado é igual ao valor
        if (toSetMethod != null) {
            if (toSetMethod.getParameterCount() != 1) {
                throw new PojofierException("'" + toSetMethod.getName() + "' method must have only one parameter.");
            }
            PojoField pojoFieldAnnotation = toField.getAnnotation(PojoField.class);
            IPojoConverter pojoFieldConverter = new PojoFieldConverter();
            IPojoRevert pojoFieldRevert = new PojoFieldRevert();
            if (status == 1) {
                if (pojoFieldAnnotation != null) {
                    try {
                        pojoFieldConverter = pojoFieldAnnotation.converter().newInstance();
                    } catch (InstantiationException | IllegalAccessException ex) {
                        ex.printStackTrace();
                        System.out.println("Falha ao instanciar o converter");
                    }
                }
                try {
                    Object value = (fromGetMethod != null) ? fromGetMethod.invoke(from) : from;
                    if (value != null && Collection.class.isAssignableFrom(value.getClass())) {
                        Collection valueCollection = (Collection) value;
                        Collection anotationsValueCollection = (Collection) value.getClass().newInstance();
                        for (Object item : valueCollection) {
                            anotationsValueCollection.add(pojoFieldConverter.convert(item));
                        }
                        toSetMethod.invoke(pojo, anotationsValueCollection);
                    } else {
                        toSetMethod.invoke(pojo, pojoFieldConverter.convert(value));
                    }
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException | NullPointerException ex) {
                    throw new PojofierException("Erro ao processar pojofy (toField: " + toField.getName() + ") " + ex.getMessage());
                }
            } else if (status == 2) {
                if (pojoFieldAnnotation != null) {
                    try {
                        pojoFieldRevert = pojoFieldAnnotation.revert().newInstance();
                    } catch (InstantiationException | IllegalAccessException ex) {
                        ex.printStackTrace();
                        System.out.println("Falha ao instanciar o converter");
                    }
                }
                try {
                    Object value = (fromGetMethod != null) ? fromGetMethod.invoke(from) : from;
                    if (value != null && Collection.class.isAssignableFrom(value.getClass())) {
                        Collection valueCollection = (Collection) value;
                        Collection anotationsValueCollection = (Collection) value.getClass().newInstance();
                        for (Object item : valueCollection) {
                            anotationsValueCollection.add(pojoFieldRevert.revert(item));
                        }
                        toSetMethod.invoke(pojo, anotationsValueCollection);
                    } else {
                        toSetMethod.invoke(pojo, pojoFieldRevert.revert(value));
                    }
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException | NullPointerException ex) {
                    throw new PojofierException("Erro ao processar pojofy (toField: " + toField.getName() + ") " + ex.getMessage());
                }
            }

        }
    }

    private static Method obtainSetMethod(Field toField, Class<?> to) throws PojofierException {
        String setMethodName = setGettfyField("set", toField.getName());
        Method toSetMethod = null;
        if (!setMethodName.contains("_persistence_") && !setMethodName.contains("SerialVersionUID")) {
            try {
                toSetMethod = to.getMethod(setMethodName, toField.getType());
            } catch (NoSuchMethodException ex) {
                throw new PojofierException(
                        "Could not identify the method '" + setMethodName + "' for setting the field '"
                        + toField.getName() + "' with parameter type '" + toField.getType().getName() + "'");
            }
        }
        return toSetMethod;
    }

    private static Method obtaionGetMethod(Field toField, Method toSetMethod, Object from) throws PojofierException {
        PojoField pojoField = toField.getAnnotation(PojoField.class);
        String fromGetMethodName;
        if (toSetMethod != null) {
            if (pojoField != null && pojoField.value().length() > 0) {
                String fromFieldName = pojoField.value();
                if (PojoField.SOURCE_OBJECT.equals(fromFieldName)) {
                    return null;
                }
                fromGetMethodName = setGettfyField("get", fromFieldName);
                //System.out.println("setGettfyField: " + fromGetMethodName);
            } else {
                fromGetMethodName = toSetMethod.getName().replace("set", "get");
                //System.out.println("get: " + fromGetMethodName);
            }
            try {
                return from.getClass().getMethod(fromGetMethodName.contains("_persistence_") ? "" : fromGetMethodName);
            } catch (NoSuchMethodException ex) {
                throw new PojofierException(
                        "Could not identify the method '" + fromGetMethodName
                        + "' to get the value for the field: '" + toField.getName() + "'."
                );
            }
        }
        return null;
    }

    private static String setGettfyField(String prefix, String fieldName) {
        return prefix + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
    }

    public static void clearContext() {
        Pojofier.context.clear();
    }

    public static void listContext() {
        for (Object key : Pojofier.context.keySet()) {
            System.out.println("Key: " + key);
        }
    }
}
