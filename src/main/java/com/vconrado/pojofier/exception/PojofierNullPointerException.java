package com.vconrado.pojofier.exception;

/**
 *
 * @author vitor
 */
public class PojofierNullPointerException extends Exception {

    public PojofierNullPointerException() {
    }

    public PojofierNullPointerException(String msg) {
        super(msg);
    }
}
