package com.vconrado.pojofier.exception;

/**
 *
 * @author vitor
 */
public class PojofierUnknownFieldException extends Exception {

    public PojofierUnknownFieldException() {
    }

    public PojofierUnknownFieldException(String msg) {
        super(msg);
    }
}
