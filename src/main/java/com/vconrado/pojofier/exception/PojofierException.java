package com.vconrado.pojofier.exception;

/**
 *
 * @author vitor
 */
public class PojofierException extends Exception {

    public PojofierException() {
    }

    public PojofierException(String msg) {
        super(msg);
    }
}
