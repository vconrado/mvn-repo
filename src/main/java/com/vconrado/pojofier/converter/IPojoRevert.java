package com.vconrado.pojofier.converter;

/**
 *
 * @author Vitor Gomes
 * @param <P>
 * @param <R>
 */
public interface IPojoRevert<P, R> {

    R revert(P p);
}
