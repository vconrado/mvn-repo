package com.vconrado.pojofier.converter;

/**
 *
 * @author Vitor Gomes
 */
public class IntegerToStringConverter implements IPojoConverter<Integer, String> {

    @Override
    public String convert(Integer p) {
        if (p == null) {
            return "";
        }
        return p.toString();
    }

}
