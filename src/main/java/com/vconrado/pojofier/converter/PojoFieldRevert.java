package com.vconrado.pojofier.converter;

/**
 *
 * @author Vitor Gomes
 * @param <P> Param type to convert method
 * @param <R> Return type to convert method
 */
public class PojoFieldRevert<P, R> implements IPojoRevert<P, R> {

    @Override
    public R revert(P p) {
        return (R) p;
    }

}
