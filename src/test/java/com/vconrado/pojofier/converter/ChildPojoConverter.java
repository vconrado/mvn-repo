package com.vconrado.pojofier.converter;

import com.vconrado.pojofier.Pojofier;
import com.vconrado.pojofier.data.Child;
import com.vconrado.pojofier.pojo.ChildPojo;

/**
 *
 * @author Vitor Gomes
 * @email vitor@ieav.cta.br
 */
public class ChildPojoConverter implements IPojoConverter<Child, ChildPojo> {

    @Override
    public ChildPojo convert(Child p) {
        ChildPojo childPojo = null;
        if (p != null) {
            childPojo = Pojofier.pojify(ChildPojo.class, p);
        }
        return childPojo;
    }

}
