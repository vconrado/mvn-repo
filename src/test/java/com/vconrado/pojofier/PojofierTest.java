/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vconrado.pojofier;

import com.vconrado.pojofier.data.Child;
import com.vconrado.pojofier.data.Data;
import com.vconrado.pojofier.data.Event;
import com.vconrado.pojofier.pojo.ChildPojo;
import com.vconrado.pojofier.pojo.DataPojo;
import com.vconrado.pojofier.pojo.EventPojo;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author vitor
 */
public class PojofierTest {

    public PojofierTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of pojify method, of class Pojofier.
     *
     * @throws java.lang.Exception
     */
    @org.junit.Test
    public void testPojify_basic() throws Exception {
        List<Child> childs = new ArrayList<>();
        childs.add(new Child(1, "1", "1"));
        childs.add(new Child(1, "1", "1"));
        childs.add(new Child(1, "1", "1"));
        Data data = new Data();
        data.setChilds(childs);
        data.setId(new Long(1));
        data.setIdade(20);
        data.setName("My Name");
        data.setUpdated(Date.valueOf("2016-01-01"));
        DataPojo pojo = Pojofier.pojify(DataPojo.class, data);
        assertEquals(pojo.getClass().getCanonicalName(), DataPojo.class.getCanonicalName());
        System.out.println("Pojo: ");
        System.out.println(pojo.toString());
    }

    @org.junit.Test
    public void testPojify_ifnullreturnnull_notnull() {
        Data data = null;
        DataPojo pojo = Pojofier.pojify(DataPojo.class, data);
        Assert.assertNotNull(pojo);
    }

    @org.junit.Test
    public void testPojify_ifnullreturnnull_null() {
        Child child = null;
        ChildPojo pojo = Pojofier.pojify(ChildPojo.class, child);
        System.out.println("Pojo: " + pojo);
        Assert.assertNull(pojo);
    }

    @org.junit.Test
    public void testPojify_object() {
        Event e = new Event();
        e.setId((long) 1);
        e.setName("Evento");
        e.setStart(new Date(0));
        e.setEnd(new Date(1234));

        EventPojo ep = Pojofier.pojify(EventPojo.class, e);

        assertEquals((long) ep.getDelay(), (long) 1234);
        System.out.println(ep);

    }

}
