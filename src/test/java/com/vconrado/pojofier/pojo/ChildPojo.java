package com.vconrado.pojofier.pojo;

import com.vconrado.pojofier.annotation.IfNullReturnNull;

/**
 *
 * @author Vitor Gomes
 * @email vitor@ieav.cta.br
 */
@IfNullReturnNull
public class ChildPojo {

    Integer id;
    String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ChildPojo{" + "id=" + id + ", name=" + name + '}';
    }

}
